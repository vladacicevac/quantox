<?php
require_once __DIR__ .'/../vendor/autoload.php';
require_once __DIR__ .'/../config/app.php';
use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;

$capsule->addConnection([
    "driver" => "mysql",
    "host" => DB_HOST,
    "database" => DB,
    "username" => DB_USER,
    "password" => DB_PASS

]);

//Make this Capsule instance available globally.
$capsule->setAsGlobal();

// Setup the Eloquent ORM.
$capsule->bootEloquent();
$capsule->bootEloquent();


$route = new Router();

$route->route('/?student=1', "StudentController@index");
$route->route('/?student=2', "StudentController@index");
$route->route('/?student=3', "StudentController@index");

$action = $_SERVER['REQUEST_URI'];


$route->dispatch($action);