<?php


class FactoryRules extends Rules
{
    public function calculate() {


        foreach ($this->board as $class => $value) {


            switch ($class) {
                case "csm":
                    $rule = new CSMRule($this->grades, $this->board);
                    break;
                case "csmb":
                    $rule = new CSMBRule($this->grades, $this->board);
                    break;
            }

        }


        $this->setPassed($rule->isPassed());
        $this->setAverage($rule->getAverage());
        $this->setGrades($rule->getGrades());
    }
}