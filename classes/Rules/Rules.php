<?php


abstract class Rules
{
    protected $grades;

    protected $average = 0;

    protected $board;

    protected $passed = false;

    public function __construct($grades, $board) {
        $this->grades = $grades;
        $this->board = $board;
        $this->calculate();
    }

    abstract public function calculate();


    public function getAverage() {
        return $this->average;
    }

    public function getBoardLimit()
    {
        $key = array_keys($this->board)[0];

        return $this->board[$key];
    }


    public function setAverage($average) {
        $this->average = $average;
    }


    public function getGrades() {
        return $this->grades;
    }


    public function setGrades($grades) {
        $this->grades = $grades;
    }


    public function isPassed() {
        return $this->passed;
    }


    public function setPassed($passed) {
        $this->passed = $passed;
    }


}