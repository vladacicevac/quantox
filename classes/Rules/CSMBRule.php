<?php


class CSMBRule extends Rules
{

    public function calculate()
    {
        $grades = $this->getGrades();

        $maxValue = 0;

        if(count($grades) > 2) {
            $value = max($grades);
        }else{
            $value = min($grades);
        }

        $this->passed = $value > $this->getBoardLimit();
        $this->average = $value;
    }
}