<?php


class CSMRule extends Rules
{
    public function calculate()
    {
        $average = 0;

        $grades = $this->getGrades();


        if(count($grades) > 0) {
            $average = array_sum($grades) / count($grades);
        }

        $this->passed = $average >= $this->getBoardLimit();
        $this->average = $average;

    }
}