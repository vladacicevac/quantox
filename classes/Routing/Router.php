<?php

class Router
{
    protected $routes = [];


    public function route($action, $route){
        $action = trim($action, '/');

        //var_dump($action);

        $this->routes[$action] = $route;
    }

    public function parseAction($route)
    {
        $data = [];
        $array = explode('@', $route);

        $data['controller'] = $array[0];
        $data['method'] = $array[1];

        return $data;
    }

    public function dispatch($action)
    {
        $action = trim($action, '/');

        $params = $this->getParams($action);

        $route = $this->routes[$action];


        $data = $this->parseAction($route);

        $controller = 'App\Controllers\\' . $data['controller'];

        $method = $data['method'];


        $router = new $controller();



        $router->$method($params);
    }

    public function getParams($action)
    {
        $param = parse_url($action);

        parse_str($param['query'], $params);

        return $params;

    }
}

