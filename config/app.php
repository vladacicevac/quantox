<?php

/**
 * Db host.
 */
define('DB_HOST', 'localhost');

/**
 * Db user.
 */
define('DB_USER', 'root');

/**
 * Db password.
 */
define('DB_PASS', 'password');

/**
 * Db name.
 */
define('DB', 'quantox_app');

define("CSM_PASS_LIMIT", 7);
define("CSMB_PASS_LIMIT", 8);
