<?php


namespace App\Controllers;


abstract class Controller
{

    public $layout = null;

    /**
     * @var string
     */
    public $view;

    /**
     * @var array
     */
    public $data = array();



    /**
     * Load view
     *
     * @param string $view
     */
    public function view($view = 'index', $data = null)
    {

    }

    public function json($data)
    {
        header('Content-Type: application/json');

        echo json_encode($data);
    }

    public function xml($data)
    {
        header('Content-type: text/xml; charset=utf-8');

        $xml = new \SimpleXMLElement('<result/>');


        foreach($data as $key=>$value) {
            $xml->addChild($key, $value);

        }

        echo $xml->asXML();

    }
}