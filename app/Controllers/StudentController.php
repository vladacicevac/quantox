<?php


namespace App\Controllers;
use App\Models\Student;
use FactoryRules;

class StudentController extends Controller
{
    public function index($params)
    {


        $student = Student::find($params['student']);

        $grades = $student->grades()->pluck('value')->toArray();

        $board = $student->board()->pluck('limit', 'name')->toArray();


        $rules = new FactoryRules($grades, $board);

        $student->grades = $rules->getGrades();
        $student->average =$rules->getAverage();
        $student->pass = $rules->isPassed() ? "PASS" : "FAIL";


        switch ($student->board->name){
            case "csm":
                return $this->json($student);
                break;

            case "csmb":
                return $this->xml($student->toArray());
                break;
        }


    }
}