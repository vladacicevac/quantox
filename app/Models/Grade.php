<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Grade extends Eloquent
{
    protected $table = "student_grades";
}