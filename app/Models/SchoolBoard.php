<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;

class SchoolBoard extends Eloquent
{
    protected $table = 'school_boards';
}