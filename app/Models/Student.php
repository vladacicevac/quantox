<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Student extends Eloquent
{
    protected $table = 'students';


    public function grades()
    {
       return $this->hasMany(Grade::class);
    }

    public function board()
    {
        return $this->belongsTo(SchoolBoard::class, 'board_id');
    }

}